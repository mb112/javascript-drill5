let cacheFunction = require('./cacheFunction')
let _Cache = cacheFunction(function () {
    return 'Hi'
})


console.log(_Cache('alpha'));
console.log(_Cache(1));
console.log(_Cache(1));
console.log(_Cache('beta'));
console.log(_Cache('beta'));
console.log(_Cache(null));
console.log(_Cache(null));
console.log(_Cache('delta'));


