function cacheFunction(cb) {
    let argumentLedger = {}
    let sequence = 0
    return function cache(a) {
        if (argumentLedger[a]) {
            return argumentLedger
        } else {
            argumentLedger[a] = sequence
            sequence += 1
            return cb()
        }
    }
}

module.exports = cacheFunction


