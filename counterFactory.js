function counterFactory(count) {
    return {
        increment: function (increaseBy) {
            return count + increaseBy
        }, decrement: function (decreaseBy) {
            return count - decreaseBy
        }
    }
}

module.exports = counterFactory

