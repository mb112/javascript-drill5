function limitFunctionCallCount(cb,n) {
    let callCount = 0
    return function callFunctionNTimes() {
        if (callCount >= n) {
            return 'Limit Exceeded'
        } else {
            callCount += 1
            return cb()
        }
    }
}

module.exports = limitFunctionCallCount
